
import Home from './Home';
import {
  About,
  NotFound,
  Navbar
} from './misc';

export {
  Home,
  About,
  NotFound,
  Navbar
};
