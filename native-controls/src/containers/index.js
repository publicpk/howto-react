/**
 * Created by peterk on 7/11/17.
 */

import HomeContainer from './Home';
import { AboutContainer } from './misc';

export {
  HomeContainer,
  AboutContainer
};
