#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function get_current_branch_name() {
    local git_dir="$1"/.git
    branch_name="$(git --git-dir=$git_dir symbolic-ref HEAD 2>/dev/null)" ||
        branch_name="(unnamed_branch)"      # detached HEAD
    branch_name=${branch_name##refs/heads/} # remove 'refs/heads/'
    echo $branch_name
}

react3in1_dir=$DIR/react3in1.git
branch_name=$(get_current_branch_name $react3in1_dir)
DEST_DIR=${1:-"${branch_name}"}
DEST_DIR=$DIR/$DEST_DIR

mkdir -p $DEST_DIR

pushd $DEST_DIR
echo "!!! Remove files under dest directory, $DEST_DIR"
rm -rf *
popd

pushd $react3in1_dir
echo ">>> Copy files to $DEST_DIR"
find ./ -type f -name "*" \
  ! -path "*/.gradle/*" \
  ! -path "*/.idea/*" \
  ! -path "*/.git/*" \
  ! -path "*/node_modules/*" \
  ! -path "*/build/*" \
  -exec cp --parents {} $DEST_DIR \;
popd

# pushd $DEST_DIR
## TODO
## go to DEST_DIR and commit it
# popd
